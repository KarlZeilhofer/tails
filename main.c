#include <stdio.h>

/**
 * opens a file for reading, and streams out new content to stdout
 * helpful for watching new data of a huge logfile
 */

/**
 * @brief main
 * @return
 */

#include <stdio.h>
#include <stdlib.h>

#include <fcntl.h> // open()
#include <unistd.h> // read()
#include <sys/time.h> // gettimeofday(...)
#include <stdint.h>
#include <stropts.h>
#include <poll.h> // see http://pubs.opengroup.org/onlinepubs/009695399/functions/poll.html
#include <stdbool.h>

bool dataAvailable();
struct pollfd fds[1]; // polled file descriptor structures

int main(int argc, char** args)
{
	if(argc != 2){
		printf("NAME\n");
		printf("\ttails - streams out new file content to stdout\n");
		printf("SYNOPSIS\n");
		printf("\ttails file\n");
		printf("DESCRIPTION\n");
		printf("\topens a file for reading, and streams out new content to stdout\n");
		printf("\thelpful for watching new data of a huge logfile\n");
		printf("AUTHOR\n");
		printf("\tKarl Zeilhofer, Team14\n");
		exit(0);
	}



	fds[0].fd = open(args[1], O_RDONLY);
	fds[0].events = POLLIN | POLLPRI; // poll for input data (normal, priority)

	lseek(fds[0].fd, 0, SEEK_END);

	if(fds[0].fd == -1){
		fprintf(stderr, "Cannot open file %s\n", args[1]);
		exit(-1);
	}

	while(1){
		if(dataAvailable()){
			static char data;

			while(read(fds[0].fd, &data, 1)){
				fwrite(&data, 1, 1, stdout);
			}
		}
		usleep(10e3);
	}
}


bool dataAvailable()
{
	int timeout_ms = 1;

	int ret = poll(fds, 1, timeout_ms);

	if(ret & POLLIN || ret & POLLPRI){
		return true;
	}else{
		return false;
	}
}

